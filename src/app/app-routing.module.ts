import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { LoginComponent } from "./pages/login/login.component";
import { GuitarCatalogueComponent } from './pages/guitar-catalogue/guitar-catalogue.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { AuthGuard } from "./gurards/auth.guard";

const routes: Routes = [
    {
        path: "",
        pathMatch: "full",
        redirectTo: "/login"
    },
    {
        path: "login",
        component: LoginComponent
    },
    {
        path: "guitars",
        component: GuitarCatalogueComponent,
        canActivate: [AuthGuard]
    },
    {
        path: "profile",
        component: ProfileComponent,
        canActivate: [AuthGuard]
    },

]

@NgModule({

    imports: [
        RouterModule.forRoot(routes)
    ],
    exports: [RouterModule]

})

export class AppRoutingModule {

}