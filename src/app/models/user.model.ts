import { Guitar } from './gituar.model';
export interface User {
    id: number;
    username: string;
    favourites: Guitar[];
} 