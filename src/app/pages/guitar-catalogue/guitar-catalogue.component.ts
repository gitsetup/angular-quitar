import { Component, OnInit } from '@angular/core';
import { GuitarCatalogueService } from './../../services/guitar-catalogue.service';
import { Guitar } from './../../models/gituar.model';

@Component({
	selector: 'app-guitar-catalogue',
	templateUrl: './guitar-catalogue.component.html',
	styleUrls: ['./guitar-catalogue.component.css'],
})
export class GuitarCatalogueComponent implements OnInit {
	get guitars(): Guitar[] {
		return this.guitarCatalogueService.guitars;
	}

	get loading(): boolean {
		return this.guitarCatalogueService.loading;
	}

  get error(): string {
    return this.guitarCatalogueService.error;
  }

	constructor(
		private readonly guitarCatalogueService: GuitarCatalogueService,
	) {}

	ngOnInit(): void {
		this.guitarCatalogueService.findAllGituars();
	}
}
