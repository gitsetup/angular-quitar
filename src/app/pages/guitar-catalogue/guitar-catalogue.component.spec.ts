import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GuitarCatalogueComponent } from './guitar-catalogue.component';

describe('GuitarCatalogueComponent', () => {
  let component: GuitarCatalogueComponent;
  let fixture: ComponentFixture<GuitarCatalogueComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GuitarCatalogueComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(GuitarCatalogueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
