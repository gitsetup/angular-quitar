import { Component, OnInit } from '@angular/core';
import { UserService } from './../../services/user.service';
import { User } from './../../models/user.model';
import { Guitar } from './../../models/gituar.model';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

get user() : User | undefined {
  return this.userSerivice.user;
}

get favourites(): Guitar[]{
  if(this.userSerivice.user){
    return this.userSerivice.user.favourites
  }
  return [];
}

  constructor(
  private readonly userSerivice:UserService,
  
  ) { }

  ngOnInit(): void {
  }

}
