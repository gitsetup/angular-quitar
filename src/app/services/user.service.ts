import { Injectable } from '@angular/core';
import { StorageKeys } from '../enum/storage-keys.enum';
import { User } from '../models/user.model';
import { StoregeUtil } from './../utils/storage.util';
import { Guitar } from './../models/gituar.model';

@Injectable({
	providedIn: 'root',
})
export class UserService {
	private _user?: User;

	public get user(): User | undefined {
		return this._user;
	}

  set user(user: User | undefined) {
    StoregeUtil.storageSave<User>(StorageKeys.User, user!);
    this._user = user;
  }

	constructor() {
		const storedUser: User | undefined = StoregeUtil.storageRead<User>(
			StorageKeys.User,
		);
		this._user = storedUser;
	}

	public inFavourites(guitarId: string) : boolean {
			if(this._user) {
				return Boolean(this.user?.favourites.find((guitar: Guitar) => guitar.id === guitarId));
			
			}

			return false;
	} 

	public addToFavourites(guitar: Guitar) : void {
	
		if(this._user){
			this._user.favourites.push(guitar);
		}
	}

	public removeFromFavourites(guitarId: string): void{
		if(this._user){
			this._user.favourites = this._user?.favourites.filter((guitar:Guitar) => guitar.id !== guitarId)
		}
	}
 }
