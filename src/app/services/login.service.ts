import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable, of, switchMap, tap } from 'rxjs';
import { StorageKeys } from '../enum/storage-keys.enum';
import { User } from '../models/user.model';
import { environment } from './../../environments/environment';
import { StoregeUtil } from './../utils/storage.util';

const { apiUser, apiKey } = environment;

@Injectable({
	providedIn: 'root',
})
export class LoginService {
	//DI
	constructor(private readonly http: HttpClient) {}

	public login(username: string): Observable<User> {
		return this.checkUsername(username).pipe(
			switchMap((user: User | undefined) => {
				if (user === undefined) {
					return this.createUser(username);
				}
				return of(user);
			})
      ,
      // tap((user: User) => {
      //     StoregeUtil.storageSave<User>(StorageKeys.User, user)
      // })
		);
	}

	private checkUsername(username: string): Observable<User | undefined> {
		return this.http.get<User[]>(`${apiUser}?username=${username}`).pipe(
			map((response: User[]) => {
				return response.pop();
			}),
		);
	}

	private createUser(username: string): Observable<User> {
		const user = {
			username,
			favourites: [],
		};

		const headers = new HttpHeaders({
			'Content-Type': 'application/json',
			'x-api-key': apiKey,
		});

		return this.http.post<User>(apiUser, user, {
			headers,
		});
	}
}
