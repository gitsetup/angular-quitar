import { Injectable } from '@angular/core';
import { GuitarCatalogueService } from './guitar-catalogue.service';
import { UserService } from './user.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Guitar } from './../models/gituar.model';
import { User } from './../models/user.model';
import { Observable, finalize, tap } from 'rxjs';

const { apiKey, apiUser } = environment;

@Injectable({
	providedIn: 'root',
})
export class FavouriteService {
	

	constructor(
		private http: HttpClient,
		private readonly userService: UserService,
		private readonly guitarService: GuitarCatalogueService,
	) {}

	//get the guitar based on the Id.

	//path request with the userId and the guitar

	public addToFavorites(guitarId: string): Observable<User> {
		if (!this.userService.user) {
			throw new Error('There is no user: addtofavoritescomp ');
		}

		const user: User = this.userService.user;
    
    
		const guitar: Guitar | undefined = this.guitarService.guitarById(guitarId);
    
    
		if (!guitar) {
			throw new Error('No guitar with id' + guitarId);
		}

		if (this.userService.inFavourites(guitarId)) {
			this.userService.removeFromFavourites(guitarId)
		} else {
      this.userService.addToFavourites(guitar)
    }

		const headers = new HttpHeaders({
			'content-type': 'application/json',
			'x-api-key': apiKey,
		});

		return this.http
			.patch<User>(`${apiUser}/${user.id}`,
				{
					favourites: [...user.favourites], //aleardy updated
				},
				{
					headers
				},
			)
			.pipe(
      tap((updatedUser: User) => {
        this.userService.user = updatedUser;
      }),
			
			) 
	}
}
