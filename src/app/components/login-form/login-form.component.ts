import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
import { LoginService } from './../../services/login.service';
import { User } from './../../models/user.model';
import { Router } from '@angular/router';
import { UserService } from './../../services/user.service';

@Component({
	selector: 'app-login-form',
	templateUrl: './login-form.component.html',
	styleUrls: ['./login-form.component.css'],
})
export class LoginFormComponent implements OnInit {

  @Output() login: EventEmitter<void> = new EventEmitter();

	constructor(
  private readonly router:Router,
  private readonly loginService: LoginService,
  private readonly userService:UserService
  ) {}

	ngOnInit(): void {}

	public loginSubmit(loginForm: NgForm): void {
		const { username } = loginForm.value;

		this.loginService.login(username).subscribe({
			next: (user: User) => {
      this.userService.user = user;
         this.login.emit();
      },
			error: () => {
        //handle localy
      },
		});
	}
}
