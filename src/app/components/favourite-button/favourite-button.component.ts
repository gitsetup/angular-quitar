import { Component, Input, OnInit } from '@angular/core';
import { FavouriteService } from './../../services/favourite.service';
import { HttpErrorResponse } from '@angular/common/http';
import { User } from './../../models/user.model';
import { UserService } from './../../services/user.service';

@Component({
	selector: 'app-favourite-button',
	templateUrl: './favourite-button.component.html',
	styleUrls: ['./favourite-button.component.css'],
})
export class FavouriteButtonComponent implements OnInit {
  
  public loading: boolean = false;
  
  public isFavourite: boolean = false;
	@Input() guitarId: string = '';

	constructor(
  private userService: UserService ,
  private readonly favouriteService: FavouriteService) {}

	ngOnInit(): void {
    this.isFavourite = this.userService.inFavourites(this.guitarId);
  }

	onFavouriteClick(): void {
	this.loading = true;
		//add the guitar to the favorites
      console.log(this.guitarId + "hello guitar id");
      
		this.favouriteService.addToFavorites(this.guitarId)
    .subscribe({
			next: (user: User) => {
			 this.loading = false;
        this.isFavourite = this.userService.inFavourites(this.guitarId);
      },
			error: (error: HttpErrorResponse) => {
				console.log('error ' , error.message);
			},
		});
	}
}
